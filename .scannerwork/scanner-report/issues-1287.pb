d
phpS107HThis function has 10 parameters, which is greater than the 7 authorized. 2��  ��
phpS1142>This function has 8 returns, which is more than the 3 allowed. 2
�� :&
$�

�� "return" statement.:&
$�

�� "return" statement.:&
$�

�� "return" statement.:&
$�

�� "return" statement.:&
$�

�� "return" statement.:&
$�

�� "return" statement.:&
$�

�� "return" statement.:&
$�

�� "return" statement.�
phpS1142>This function has 4 returns, which is more than the 3 allowed. 2
��  :&
$�

�� "return" statement.:&
$�

�� "return" statement.:&
$�

�� "return" statement.:&
$�

�� "return" statement.�
phpS3776TRefactor this function to reduce its Cognitive Complexity from 35 to the 15 allowed. )      4@2
�� :
�

�� +1:
�

�� +1:
�

�� 
+1:
�

�� 
+1:
�

�� 
+1:+
)�

�� +2 (incl. 1 for nesting):+
)�

�� +2 (incl. 1 for nesting):+
)�

�� +2 (incl. 1 for nesting):+
)�

�� +3 (incl. 2 for nesting):+
)�

�� +2 (incl. 1 for nesting):
�

��
 +1:+
)�

�� +2 (incl. 1 for nesting):
�

�� +1:+
)�

�� +2 (incl. 1 for nesting):+
)�

�� +3 (incl. 2 for nesting):
�

�� +1:
�

��5 7+1:
�

�� +1:+
)�

�� +3 (incl. 2 for nesting):
�

�� +1:+
)�

�� +2 (incl. 1 for nesting):
�

�� 
+1�
phpS3776TRefactor this function to reduce its Cognitive Complexity from 61 to the 15 allowed. )      G@2
�� :
�

�� +1:+
)�

�� +2 (incl. 1 for nesting):
�

�� 
+1:
�

�� +1:+
)�

�� +2 (incl. 1 for nesting):
�

�� +1:
�

�� +1:
�

��
 +1:
�

�� 
+1:
�

�� 
+1:
�

�� +1:
�

�� 
+1:
�

�� 
+1:
�

�� +1:
�

�� 
+1:
�

�� +1:
�

�� 
+1:
�

�� 
+1:
�

�� +1:
�

�� 
+1:
�

�� 
+1:
�

�� 
+1:
�

�� +1:+
)�

�� +2 (incl. 1 for nesting):+
)�

�� +2 (incl. 1 for nesting):+
)�

�� +3 (incl. 2 for nesting):+
)�

��@ A+2 (incl. 1 for nesting):
�

��' )+1:+
)�

��3 4+2 (incl. 1 for nesting):+
)�

�� +2 (incl. 1 for nesting):
�

�� +1:+
)�

�� +3 (incl. 2 for nesting):+
)�

�� +2 (incl. 1 for nesting):
�

�� +1:+
)�

�� +3 (incl. 2 for nesting):+
)�

�� +2 (incl. 1 for nesting):
�

�� +1:+
)�

�� +3 (incl. 2 for nesting):+
)�

�� +2 (incl. 1 for nesting):+
)�

�� +2 (incl. 1 for nesting):+
)�

�� +2 (incl. 1 for nesting)�
phpS1192eDefine a constant instead of duplicating this literal "}} & \\multicolumn{1}{|c|}{\\textbf{" 3 times. )      @2
�� 4:
�

�� 4Duplication.:
�

�� 4Duplication.�
phpS1192LDefine a constant instead of duplicating this literal " \\caption{" 3 times. )      @2
�� $:
�

�� $Duplication.:
�

�� $Duplication.G
phpS1172+Remove the unused function parameter "$db". 2
��  #G
phpS1172+Remove the unused function parameter "$db". 2
��  #N
phpS11722Remove the unused function parameter "$error_url". 2
��0 :N
phpS11722Remove the unused function parameter "$error_url". 2
��5 ?K
phpS1172.Remove the unused function parameter "$dates". 2�� �R
phpS11724Remove the unused function parameter "$export_mode". 2��� �R
phpS11724Remove the unused function parameter "$export_type". 2��� �K
phpS1066/Merge this if statement with the enclosing one. 2
�� K
phpS1066/Merge this if statement with the enclosing one. 2
�� S
phpS11557Use empty() to check whether the array is empty or not. 2
�� /�
phpS4144aUpdate this method so that its implementation is not identical to "PMA_exportFooter" on line 111. 2
�� :(
&�
oo original implementation�
phpS4144aUpdate this method so that its implementation is not identical to "PMA_exportFooter" on line 111. 2
�� :(
&�
oo original implementation�
phpS1788iMove arguments "$do_relation", "$do_comments", "$do_mime", "$dates" after arguments without default value 2��  �